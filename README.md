# Rock Paper Scissors MVP app

MVP version of a Rock-Paper-Scissors game:
* basic 'player vs computer' functionality to illustrate game dynamics (i.e. shape selection, output of game results)
* basic layout and styling, just enough to illustrate layout and UI feedback mechanisms
* basic gamification in form of breakdown of percentage of rounds won by each party
* responsive layout
* file structure to support further codebase growth

## Demo
Visit [http://www.nrdcp.net/rps](http://www.nrdcp.net/rps) to see a live project demo.

## Getting Started
### Dependencies
Tools needed to run this app:
* `node` and `npm`

### Installing
* `fork` this repo
* `clone` your fork
* `npm install -g gulp karma karma-cli webpack` install global cli dependencies
* `npm install` to install dependencies
 
#### Gulp Tasks
Here's a list of available tasks:
* `serve`
  * starts a dev server via `webpack-dev-server`, serving the `src` folder.
* `webpack`
  * runs Webpack, which will transpile, concatenate, and compress (collectively, "bundle") all assets and modules into `dist/bundle.js`. It also prepares `index.html` to be used as application entry point, links assets and created dist version of our application.
