
class GameController {
  constructor($timeout, GameService, GameShapes) {
    'ngInject';

    this.$timeout = $timeout;
    this.GameShapes = GameShapes;
    this.GameService = GameService;

    this.round = this.GameService.create();
    this.roundStates = {
      active: false
    }
    this.lastSelectedUserShapeId = null;
    this.shapeOptions = this.GameShapes.get();
  }

  throwShape(shapeId) {
    if (this.roundStates.active) {
      return;
    }

    this.roundStates.active = true;
    this.lastSelectedUserShapeId = shapeId;

    this.GameService.setUserShape(shapeId)
      .then(() => {
        return this.GameService.getRoundWinner();
      })
      .then(() => {
        this.$timeout(() => {
          this.round = this.GameService.create();
          this.roundStates.active = false;
          this.lastSelectedUserShapeId = null;
        }, 1500);
      });
  }
}

export default GameController;
