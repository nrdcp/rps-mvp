import GameRoundModel from './game-round.model';

class GameService {
  constructor($q, GameShapes, GameLogService, Utils) {
    'ngInject';

    this.$q = $q;
    this.GameLogService = GameLogService;
    this.GameShapes = GameShapes;
    this.Utils = Utils;

    this.rounds = this.GameLogService.get();
    this.currentRound = null;
    this.shapes = {
      computer: null,
      user: null
    }
  }

  create() {
    this.currentRound = new GameRoundModel(this.rounds.length + 1);
    return this.currentRound;
  }

  setUserShape(userShapeId) {
    let deferred = this.$q.defer();
    
    this.getComputerShape()
      .then((computerShapeId) => {
        this.currentRound.shapes.user = userShapeId;
        this.currentRound.shapes.computer = computerShapeId;
        deferred.resolve();
      });

    return deferred.promise;
  }

  getComputerShape() {
    // machine-learning? eeh random for now
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let generatedPos = this.Utils.getRandomIntInclusive(0, this.GameShapes.get().length-1);
        resolve(this.GameShapes.get()[generatedPos].id);
      }, 600);
    });
  }

  getRoundWinner() {
    let deferred = this.$q.defer();

    this.computeRoundWinner()
      .then(() => {
        this.GameLogService.add(this.currentRound);
        return deferred.resolve();
      })
      .catch((e) => {
        return deferred.reject();
      });

    return deferred.promise;
  }

  computeRoundWinner() {
    if (!this.currentRound.shapes.user || !this.currentRound.shapes.computer) {
      return Promise.reject();
    }

    if (this.currentRound.shapes.user == this.currentRound.shapes.computer) {
      this.currentRound.winner = 'none';
      return Promise.resolve();
    }

    let outcome;
    switch(this.currentRound.shapes.user) {
      case 'rock':
        outcome = this.currentRound.shapes.computer == 'scissors';
        break;
      case 'paper':
        outcome = this.currentRound.shapes.computer == 'rock';
        break;
      case 'scissors':
        outcome = this.currentRound.shapes.computer == 'paper';
        break;
    }

    this.currentRound.winner = outcome ? 'user' : 'computer';
    return Promise.resolve();
  }
}

export default GameService;
