
class GameRoundModel {
  constructor(id) {
    this.id = id;
    this.shapes = {
      computer: null,
      user: null
    };
    this.winner = null;
  }
}

export default GameRoundModel;
