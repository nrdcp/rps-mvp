
const shapes = [
  {
    id: 'rock',
    name: 'Rock' 
  },
  {
    id: 'paper',
    name: 'Paper' 
  },
  {
    id: 'scissors',
    name: 'Scissors' 
  }
];

function get() {
  return shapes;
}

export default function() {
  return {
    get
  }
};
