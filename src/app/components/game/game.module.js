import angular from 'angular';
import gameComponent from './game.component';
import GameService from './game.service';
import gameLogComponent from './game-log/game-log.component';
import GameLogService from './game-log/game-log.service';
import GameShapesService from './game-shapes.service';

let gameModule = angular.module('game', [])

.component('game', gameComponent)
.component('gameLog', gameLogComponent)
.service('GameService', GameService)
.service('GameLogService', GameLogService)
.factory('GameShapes', GameShapesService)

.name;

export default gameModule;
