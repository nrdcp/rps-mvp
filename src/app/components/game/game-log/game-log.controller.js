
class GameLogController {
  constructor($scope, GameLogService) {
    'ngInject';

    this.$scope = $scope;
    this.GameLogService = GameLogService;

    this.log = this.GameLogService.get();
    this.user = null;
    this.computer = null;
  }

  getPlayersStats() {
    this.user = this.GameLogService.getPlayerStats('user');
    this.computer = this.GameLogService.getPlayerStats('computer');
  }

  $onInit() {
    this.$scope.$watch(() => this.log.length, () => {
      this.getPlayersStats();
    });
  }
}

export default GameLogController;
