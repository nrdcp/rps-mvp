
class GameLogService {
  constructor() {
    this.log = [];
  }

  add(logEntry) {
    this.log.push(logEntry);
  }

  get() {
    return this.log;
  }

  getPlayerStats(playerId) {
    let numRoundsWon = this.log.filter((logEntry) => logEntry.winner === playerId).length;
    let totalRoundsSingleWinner = this.log.filter((logEntry) => logEntry.winner !== 'none').length;

    return {
      numRoundsWon: numRoundsWon,
      percentRoundsWon: totalRoundsSingleWinner ? Math.round((numRoundsWon/totalRoundsSingleWinner)*100) : 50
    }
  }

}

export default GameLogService;
