import template from './game-log.html';
import controller from './game-log.controller';
import './game-log.scss';

let gameLogComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default gameLogComponent;
