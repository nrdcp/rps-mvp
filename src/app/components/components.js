import angular from 'angular';
import HomeModule from './home/home.module';
import GameModule from './game/game.module';

let componentModule = angular.module('app.components', [
  HomeModule,
  GameModule
])
  
.name;

export default componentModule;
