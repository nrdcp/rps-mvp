import angular from 'angular';

import UtilsService from './services/utils.service';

let commonModule = angular.module('app.common', [])

.factory('Utils', UtilsService)
  
.name;

export default commonModule;
