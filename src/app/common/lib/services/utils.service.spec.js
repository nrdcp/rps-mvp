import UtilsService from './utils.service';

describe('UtilsService', () => {
  let utilsService = UtilsService();

  describe('getRandomIntInclusive', () => {
    it('returns a random integer within bounds', () => {
      let result = utilsService.getRandomIntInclusive(2,5);
      expect(result).to.be.above(1.99);
      expect(result).to.be.below(5.01);
    });
  });
});